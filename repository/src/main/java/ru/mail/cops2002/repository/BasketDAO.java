package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.Basket;

public interface BasketDAO extends DAO<Basket, Integer>{

    Basket getBasketByUserId(Integer Id);

    void changeBasketCostByBasketId(Integer cost, Integer id);
}
