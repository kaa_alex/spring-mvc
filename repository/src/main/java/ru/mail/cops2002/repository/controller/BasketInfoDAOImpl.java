package ru.mail.cops2002.repository.controller;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.BasketInfoDAO;
import ru.mail.cops2002.repository.models.BasketInfo;

@Repository
public class BasketInfoDAOImpl extends DAOImpl<BasketInfo, Integer> implements BasketInfoDAO {

    @Override
    public BasketInfo getBasketInfoByBasketId(Integer id) {
        return null;
    }

    @Override
    public void deleteByArticul(Integer articul, Integer basketId) {
        String queryString = "DELETE FROM BasketInfo WHERE position_articul=:articul and basket_id=:basketId";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("articul", articul);
        query.setParameter("basketId", basketId);
        query.executeUpdate();
    }

    @Override
    public void changePositionNumberByArticul(Integer articul, Integer basketId, Integer positionNumber) {
        String queryString = "UPDATE BasketInfo SET position_number=:positionNumber WHERE position_articul=:articul and basket_id=:basketId";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("articul", articul);
        query.setParameter("basketId", basketId);
        query.setParameter("positionNumber", positionNumber);
        query.executeUpdate();
    }

    @Override
    public void cleanBasket(Integer basketId) {
        String queryString = "DELETE FROM BasketInfo WHERE basket_id=:basketId";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("basketId", basketId);
        query.executeUpdate();
    }
}
