package ru.mail.cops2002.repository.controller;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.OrderInfoDAO;
import ru.mail.cops2002.repository.models.OrderInfo;

import java.util.List;

@Repository
public class OrderInfoDAOImpl extends DAOImpl<OrderInfo, Integer> implements OrderInfoDAO {
    @Override
    public void deleFromOrderById(Integer Id, Integer articul) {
        String queryString = "DELETE FROM OrderInfo WHERE position_articul=:articul and order_id=:orderId";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("articul", articul);
        query.setParameter("orderId", Id);
        query.executeUpdate();
    }

    @Override
    public void changePositionNumberById(Integer positionArticul, Integer orderId, Integer positionNumber) {
        String queryString = "UPDATE OrderInfo SET position_number=:positionNumber WHERE position_articul=:articul and order_id=:orderId";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("articul", positionArticul);
        query.setParameter("orderId", orderId);
        query.setParameter("positionNumber", positionNumber);
        query.executeUpdate();
    }

    @Override
    public List<OrderInfo> getOrderInfoByArticul(Integer articul) {
        String queryString = "from OrderInfo where position_articul=:articul";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("articul", articul);
        List list = query.getResultList();
        return list != null ? list : null;
    }
}
