package ru.mail.cops2002.repository.controller;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.OrderDAO;
import ru.mail.cops2002.repository.models.Order;

import java.util.List;

@Repository
public class OrderDAOImpl extends DAOImpl<Order, Integer> implements OrderDAO {

    @Override
    public List<Order> getOrderbyUserId(Integer id) {
        // TODO Auto-generated method stub
        String queryString = "from Order where user_id=:id";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("id", id);
        List list = query.getResultList();
        return list != null ? list : null;
    }

    @Override
    public void changeOrderCostByOrderId(Integer cost, Integer id) {
        String queryString = "UPDATE Order SET order_cost=:cost WHERE order_id=:id";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("cost", cost);
        query.setParameter("id", id);
        query.executeUpdate();
    }

}

