package ru.mail.cops2002.repository.controller;

import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.UserInfoDAO;
import ru.mail.cops2002.repository.models.UserInfo;

@Repository
public class UserInfoDAOImpl extends DAOImpl<UserInfo, Integer> implements UserInfoDAO {
}
