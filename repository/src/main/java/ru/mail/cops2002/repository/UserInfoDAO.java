package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.UserInfo;

public interface UserInfoDAO extends DAO<UserInfo, Integer> {
}
