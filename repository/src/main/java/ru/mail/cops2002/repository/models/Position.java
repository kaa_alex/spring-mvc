package ru.mail.cops2002.repository.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "price")

public class Position implements Serializable {

    private static final long serialVersionUID = 3385974054619939924L;
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "position_articul", unique = true, nullable = false)
    private Integer articul;
    @Column(name = "position_cost", nullable = false)
    private Integer cost;
    @Column(name = "position_description", nullable = false)
    private String description;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "position", cascade = CascadeType.ALL)
    private BasketInfo binfo;

    public Integer getArticul() {
        return articul;
    }

    public void setArticul(Integer articul) {
        this.articul = articul;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BasketInfo getBinfo() {
        return binfo;
    }

    public void setBinfo(BasketInfo binfo) {
        this.binfo = binfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return Objects.equals(articul, position.articul);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articul);
    }
}
