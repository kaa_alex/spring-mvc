package ru.mail.cops2002.repository.models;

public enum OrderStatus {

    NEW,REVIEWING,IN_PROGRESS,DELIVERED;

}
