package ru.mail.cops2002.repository.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "basket")

public class Basket implements Serializable {

    private static final long serialVersionUID = -8122651329640974178L;
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "basket_id", unique = true, nullable = false)
    private Integer id;
    @Column(name = "basket_cost")
    private Integer cost = 0;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", unique = true, nullable = false)
    private User user;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "basket", cascade = CascadeType.ALL)
    private Set<BasketInfo> positions = new HashSet<BasketInfo>(0);

    public Set<BasketInfo> getPositions() {
        return positions;
    }

    public void setPositions(Set<BasketInfo> positions) {
        this.positions = positions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Basket)) return false;
        Basket basket = (Basket) o;
        return Objects.equals(user, basket.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }
}
