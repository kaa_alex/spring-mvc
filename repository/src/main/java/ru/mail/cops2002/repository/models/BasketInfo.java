package ru.mail.cops2002.repository.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "basket_info")

public class BasketInfo implements Serializable {

    private static final long serialVersionUID = 3385974054619939924L;
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "position_articul")
    private Position position;
    @Column(name = "position_number")
    private Integer number;
    @ManyToOne
    @JoinColumn(name = "basket_id")
    private Basket basket;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketInfo)) return false;
        BasketInfo info = (BasketInfo) o;
        return Objects.equals(position, info.position) &&
                Objects.equals(basket, info.basket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, basket);
    }
}