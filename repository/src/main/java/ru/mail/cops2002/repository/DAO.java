package ru.mail.cops2002.repository;


import java.io.Serializable;
import java.util.List;

public interface DAO<O extends Serializable, ID extends Serializable> {

    ID save(O entity);

    void saveOrUpdate(O entity);

    void update(O entity);

    O getById(ID id);

    List<O> getAll();

    void deleteById(ID id);

    void deleteObject(O entity);

    void deleteAll();
}
