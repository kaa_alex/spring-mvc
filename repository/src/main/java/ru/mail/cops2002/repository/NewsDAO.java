package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.News;

public interface NewsDAO extends DAO<News, Integer> {
}
