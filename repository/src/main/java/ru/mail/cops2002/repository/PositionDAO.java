package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.Position;

public interface PositionDAO extends DAO<Position, Integer>{

    Position getByPositionByArticul(Integer articul);

}
