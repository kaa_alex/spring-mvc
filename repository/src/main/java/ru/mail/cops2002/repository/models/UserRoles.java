package ru.mail.cops2002.repository.models;

public enum UserRoles {

	ROLE_ADMIN, ROLE_USER, ROLE_SUPERADMIN;

}
