package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.OrderInfo;

import java.util.List;

public interface OrderInfoDAO extends DAO<OrderInfo, Integer> {

    void deleFromOrderById(Integer Id, Integer articul);

    void changePositionNumberById(Integer positionArticul, Integer orderId, Integer number);

    List<OrderInfo> getOrderInfoByArticul(Integer articul);
}
