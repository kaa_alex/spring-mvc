package ru.mail.cops2002.repository.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.DAO;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public abstract class DAOImpl<O extends Serializable, ID extends Serializable> implements DAO<O, ID> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<O> entityClass;

    public DAOImpl() {
        this.entityClass = (Class<O>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public ID save(O entity) {
        // TODO Auto-generated method stub
        return (ID) getSession().save(entity);
    }

    @Override
    public void saveOrUpdate(O entity) {
        // TODO Auto-generated method stub
        getSession().saveOrUpdate(entity);
    }

    @Override
    public void update(O entity) {
        // TODO Auto-generated method stub
        getSession().update(entity);
    }

    @Override
    public O getById(ID id) {
        // TODO Auto-generated method stub
        return (O) getSession().get(this.entityClass, id);
    }

    @Override
    public List<O> getAll() {
        // TODO Auto-generated method stub
        return getSession().createCriteria(this.entityClass).list();
    }

    @Override
    public void deleteById(ID id) {
        // TODO Auto-generated method stub
        O object = getById(id);
        getSession().delete(object);
    }

    @Override
    public void deleteObject(O entity) {
        // TODO Auto-generated method stub
        getSession().delete(entity);
    }

    @Override
    public void deleteAll() {
        // TODO Auto-generated method stub
        List<O> entities = getAll();
        for (O entity : entities) {
            getSession().delete(entity);
        }
    }
}
