package ru.mail.cops2002.repository.controller;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.BasketDAO;
import ru.mail.cops2002.repository.models.Basket;

@Repository
public class BasketDAOImpl extends DAOImpl<Basket, Integer> implements BasketDAO {

    @Override
    public Basket getBasketByUserId(Integer id) {
        String queryString = "from Basket where user_id=:id";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("id", id);
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (Basket) uniqueResult : null;
    }

    @Override
    public void changeBasketCostByBasketId(Integer cost, Integer id) {
        String queryString = "UPDATE Basket SET basket_cost=:cost WHERE basket_id=:id";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("cost", cost);
        query.setParameter("id", id);
        query.executeUpdate();
    }
}

