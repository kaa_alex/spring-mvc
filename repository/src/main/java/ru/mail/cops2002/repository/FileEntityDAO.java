package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.FileEntity;

public interface FileEntityDAO extends DAO<FileEntity, Integer> {
}
