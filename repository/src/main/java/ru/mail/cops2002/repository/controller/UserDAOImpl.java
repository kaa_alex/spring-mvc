package ru.mail.cops2002.repository.controller;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;
import ru.mail.cops2002.repository.UserDAO;
import ru.mail.cops2002.repository.models.User;

@Repository
public class UserDAOImpl extends DAOImpl<User, Integer> implements UserDAO {

	@Override
    @Transactional
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		String queryString = "from User where user_email=:email";
        Query<?> query = getSession().createQuery(queryString);
        query.setParameter("email", email);
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (User) uniqueResult : null;
	}

}
