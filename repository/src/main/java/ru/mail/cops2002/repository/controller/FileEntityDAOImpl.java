package ru.mail.cops2002.repository.controller;

import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.FileEntityDAO;
import ru.mail.cops2002.repository.models.FileEntity;

@Repository
public class FileEntityDAOImpl extends DAOImpl<FileEntity, Integer> implements FileEntityDAO {
}
