package ru.mail.cops2002.repository.controller;

import org.springframework.stereotype.Repository;
import ru.mail.cops2002.repository.NewsDAO;
import ru.mail.cops2002.repository.models.News;

@Repository
public class NewsDAOImpl extends DAOImpl<News, Integer> implements NewsDAO {
}
