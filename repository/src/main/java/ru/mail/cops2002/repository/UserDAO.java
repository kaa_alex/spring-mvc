package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.User;

public interface UserDAO extends DAO<User, Integer> {

	User getUserByEmail(String email);

}
