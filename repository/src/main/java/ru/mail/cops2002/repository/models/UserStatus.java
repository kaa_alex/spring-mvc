package ru.mail.cops2002.repository.models;

public enum UserStatus {

    ACTIVE, BLOCKED;
}
