package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.BasketInfo;

public interface BasketInfoDAO extends DAO<BasketInfo, Integer> {

    BasketInfo getBasketInfoByBasketId(Integer id);

    void cleanBasket(Integer basketId);

    void deleteByArticul(Integer articul, Integer id);

    void changePositionNumberByArticul(Integer articul, Integer basketId, Integer positionNumber);
}
