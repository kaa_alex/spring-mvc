package ru.mail.cops2002.repository;

import ru.mail.cops2002.repository.models.Order;

import java.util.List;

public interface OrderDAO extends DAO<Order, Integer> {

    List<Order> getOrderbyUserId(Integer Id);

    void changeOrderCostByOrderId(Integer cost, Integer id);

}
