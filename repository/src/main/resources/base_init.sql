INSERT INTO Price(position_articul,position_description,position_cost) VALUES (1,"Drinking Water 0.5L", 1);

INSERT INTO User (user_id,user_email,user_password,user_status,user_role) VALUES (1,"admin@admin.ru","$2a$04$yWrzp2kQBeKBsyBID.hW/OENEv0CjztLqVjeNxI1bVtxpEWXktyLK","ACTIVE","ROLE_ADMIN");
INSERT INTO User_information(user_id,first_name,middle_name,last_name,phone,adress) VALUES (1,"admin","admin","admin","admin","admin");

INSERT INTO User (user_id,user_email,user_password,user_status,user_role) VALUES (2,"superadmin@admin.ru","$2a$04$TxYXJNk3sPaQtZKodpsZn.58nI44i00Kd0UwAqEKFT45jyJvK0SQy","ACTIVE" ,"ROLE_SUPERADMIN");
INSERT INTO User_information(user_id,first_name,middle_name,last_name,phone,adress) VALUES (2,"superadmin","superadmin","superadmin","superadmin","superadmin");