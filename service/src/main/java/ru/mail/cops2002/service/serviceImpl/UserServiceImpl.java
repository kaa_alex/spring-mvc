package ru.mail.cops2002.service.serviceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.cops2002.repository.UserDAO;
import ru.mail.cops2002.repository.UserInfoDAO;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.repository.models.UserInfo;
import ru.mail.cops2002.repository.models.UserRoles;
import ru.mail.cops2002.repository.models.UserStatus;
import ru.mail.cops2002.service.UserService;
import ru.mail.cops2002.service.models.UserDTO;
import ru.mail.cops2002.service.util.UserConverter;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserInfoDAO userInfoDAO;

    @Autowired
    public UserServiceImpl(
            UserDAO userDao,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            UserInfoDAO userInfoDAO
    ) {
        this.userInfoDAO = userInfoDAO;
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public void saveUser(UserDTO userDTO) {
        User user = UserConverter.convert(userDTO);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setRole(UserRoles.ROLE_USER);
        user.setStatus(UserStatus.ACTIVE);
        userDao.save(user);
    }

    @Override
    @Transactional
    public List<UserDTO> getAll() {
        List<User> users = userDao.getAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : users) {
            userDTOList.add(UserConverter.convert(user));
        }
        return userDTOList;
    }

    @Override
    @Transactional
    public UserDTO getUserById(Integer id) {
        UserDTO userDTO = null;
        User user = userDao.getById(id);
        if (user != null) {
            userDTO = UserConverter.convert(user);
        }
        return userDTO;
    }

    @Override
    @Transactional
    public void saveOrUpdateUser(UserDTO userDTO) {
        User user = UserConverter.convert(userDTO);
        userDao.saveOrUpdate(user);
    }

    @Override
    @Transactional
    public void saveUserChanges(UserDTO userDTO) {
        User user = userDao.getById(userDTO.getId());
        user.setStatus(userDTO.getStatus());
        user.setRole(userDTO.getRole());
        userDao.saveOrUpdate(user);
    }

    @Override
    @Transactional
    public void changePassword(UserDTO userDTO, String password) {
        User user = userDao.getById(userDTO.getId());
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setStatus(userDTO.getStatus());
        user.setRole(userDTO.getRole());
        userDao.saveOrUpdate(user);
    }

    @Override
    @Transactional
    public void saveUserInfoChanges(UserDTO userDTO) {
        User user = UserConverter.convert(userDTO);
        UserInfo info = user.getInfo();
        info.setId(user.getId());
        userInfoDAO.saveOrUpdate(info);
    }

    @Override
    @Transactional
    public void updateUserPassword(UserDTO userDTO, String password) {
        User user = userDao.getById(userDTO.getId());
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userDao.saveOrUpdate(user);
    }
}
