package ru.mail.cops2002.service.util;

import ru.mail.cops2002.repository.models.Order;
import ru.mail.cops2002.repository.models.OrderInfo;
import ru.mail.cops2002.service.models.OrderDTO;
import ru.mail.cops2002.service.models.PositionDTO;

import java.util.Map;

public class OrderConverter {

    private OrderConverter() {
    }

    public static Order convert(OrderDTO orderDTO) {
        Order order = new Order();
        order.setId(orderDTO.getOrderId());
        order.setCost(orderDTO.getCost());
        order.setStatus(orderDTO.getStatus());
        order.setUser(UserConverter.convert(orderDTO.getUserDTO()));
        for (Map.Entry<PositionDTO, Integer> position : orderDTO.getPositions().entrySet()) {
            OrderInfo info = new OrderInfo();
            info.setOrder(order);
            info.setPosition(PositionConverter.convert(position.getKey()));
            info.setNumber(position.getValue());
            order.getPositions().add(info);
        }
        return order;
    }

    public static OrderDTO convert(Order order) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderId(order.getId());
        orderDTO.setCost(order.getCost());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setUserDTO(UserConverter.convert(order.getUser()));
        orderDTO.setUserId(order.getUser().getId());
        for (OrderInfo info : order.getPositions()) {
            PositionDTO position = PositionConverter.convert(info.getPosition());
            orderDTO.getPositions().put(position, info.getNumber());
        }
        return orderDTO;
    }
}
