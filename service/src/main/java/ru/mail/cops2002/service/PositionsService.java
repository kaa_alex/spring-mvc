package ru.mail.cops2002.service;

import ru.mail.cops2002.service.models.PositionDTO;

import java.util.List;

public interface PositionsService {

    void savePosition(PositionDTO positionDTO);

    List<PositionDTO> getAll();

    void copyPosition(PositionDTO positionDTO);

    PositionDTO getPositionById(Integer articul);

    void updatePosition(PositionDTO positionDTO);

    boolean checkPositionEditable(Integer positionArticul);

}
