package ru.mail.cops2002.service;

import ru.mail.cops2002.service.models.NewsDTO;

import java.io.IOException;
import java.util.List;

public interface NewsService {

    List<NewsDTO> getAll();

    NewsDTO getById(Integer id);

    void save(NewsDTO newsDTO, String path) throws IOException;

    void update(NewsDTO newsDTO, String filepath) throws IOException;

    void delete(Integer id) throws IOException;

    void updateWithoutPicture(NewsDTO newsDTO) throws IOException;
}
