package ru.mail.cops2002.service.util;

import ru.mail.cops2002.repository.models.News;
import ru.mail.cops2002.service.models.NewsDTO;

public class NewsConverter {

    private NewsConverter() {
    }

    public static News convert(News news, NewsDTO newsDTO) {
        news.setHeader(newsDTO.getHeader());
        news.setContent(newsDTO.getContent());
        return news;
    }

    public static NewsDTO convert(News news) {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setContent(news.getContent());
        newsDTO.setDate(news.getContent());
        newsDTO.setHeader(news.getHeader());
        newsDTO.setNewsId(news.getId());
        newsDTO.setFileName(news.getFileEntity().getFilename());
        newsDTO.setFileLocation(news.getFileEntity().getLocation());
        return newsDTO;
    }

    public static News convert(NewsDTO newsDTO) {
        News news = new News();
        news.setHeader(newsDTO.getHeader());
        news.setContent(newsDTO.getContent());
        news.setDate(newsDTO.getDate());
        return news;
    }
}
