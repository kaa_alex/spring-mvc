package ru.mail.cops2002.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import ru.mail.cops2002.repository.NewsDAO;
import ru.mail.cops2002.repository.models.FileEntity;
import ru.mail.cops2002.repository.models.News;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.service.NewsService;
import ru.mail.cops2002.service.models.AppUserPrincipal;
import ru.mail.cops2002.service.models.NewsDTO;
import ru.mail.cops2002.service.util.NewsConverter;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class NewsServiceImpl implements NewsService {


    private final NewsDAO newsDAO;
    private Properties properties;

    @Autowired
    public NewsServiceImpl(NewsDAO newsDAO, Properties properties) {
        this.newsDAO = newsDAO;
        this.properties = properties;
    }

    @Override
    @Transactional
    public List<NewsDTO> getAll() {
        List<News> newsList = newsDAO.getAll();
        List<NewsDTO> newsDTOList = new ArrayList<>();
        for (News news : newsList) {
            NewsDTO newsDTO = NewsConverter.convert(news);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }

    @Override
    @Transactional
    public NewsDTO getById(Integer id) {
        News news = newsDAO.getById(id);
        NewsDTO newsDTO = null;
        if (news != null) {
            newsDTO = NewsConverter.convert(news);
        }
        return newsDTO;
    }

    @Override
    @Transactional
    public void save(NewsDTO newsDTO, String path) throws IOException {
        Date date = new Date();
        newsDTO.setDate(date.toString());
        News news = NewsConverter.convert(newsDTO);
        try {
            FileEntity fileEntity = getFileEntity(newsDTO, path);
            news.setFileEntity(fileEntity);
            AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = userPrincipal.getUser();
            news.setUser(user);
            newsDAO.save(news);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public void update(NewsDTO newsDTO, String filepath) throws IOException {
        News news = NewsConverter.convert(newsDAO.getById(newsDTO.getNewsId()), newsDTO);
        try {
            FileEntity fileEntity = getFileEntity(newsDTO, filepath);
            news.getFileEntity().setLocation(fileEntity.getLocation());
            news.getFileEntity().setFilename(fileEntity.getFilename());
            newsDAO.saveOrUpdate(news);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public void delete(Integer id) {

        if (id > 0) {
            News news = newsDAO.getById(id);
            newsDAO.deleteObject(news);
        }
    }

    @Override
    @Transactional
    public void updateWithoutPicture(NewsDTO newsDTO) throws IOException {
        News news = NewsConverter.convert(newsDAO.getById(newsDTO.getNewsId()), newsDTO);
        Date date = new Date();
        news.setDate(date.toString());
        newsDAO.update(news);
    }

    private FileEntity getFileEntity(NewsDTO newsDTO, String path) throws IOException {
        String filename = System.currentTimeMillis() + ".jpg";
        String fileLocation = path + filename;
        FileCopyUtils.copy(newsDTO.getFile().getBytes(), new File(fileLocation));
        FileEntity fileEntity = new FileEntity();
        fileEntity.setLocation(fileLocation);
        fileEntity.setFilename(filename);
        return fileEntity;
    }
}
