package ru.mail.cops2002.service.models;

public class PositionDTO {

    private Integer articul;
    private Integer cost;
    private java.lang.String description;

    public Integer getArticul() {
        return articul;
    }

    public void setArticul(Integer articul) {
        this.articul = articul;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PositionDTO)) return false;

        PositionDTO that = (PositionDTO) o;

        if (articul != null ? !articul.equals(that.articul) : that.articul != null) return false;
        if (cost != null ? !cost.equals(that.cost) : that.cost != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = articul != null ? articul.hashCode() : 0;
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
