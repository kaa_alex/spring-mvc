package ru.mail.cops2002.service;

import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.OrderDTO;

import java.util.List;

public interface OrderService {

    void saveOrderByBasket(BasketDTO basketDTO);

    List<OrderDTO> getAll();

    List<OrderDTO> getAllById(Integer id);

    OrderDTO getOrderById(Integer id);

    void SaveOrUpdate(OrderDTO orderDTO);

    void deleteFromOrderById(Integer orderId, Integer positionArticul);

    void changePositionNumberByArticul(Integer positionArticul, Integer orderId, Integer positionNumber);

    Integer getOrderCostByOrderId(Integer id);

    void deleteOrderById(Integer id);
}
