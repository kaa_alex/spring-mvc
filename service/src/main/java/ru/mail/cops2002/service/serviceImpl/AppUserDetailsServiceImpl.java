package ru.mail.cops2002.service.serviceImpl;

import ru.mail.cops2002.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.service.models.AppUserPrincipal;

@Service(value = "appUserDetailsService")
public class AppUserDetailsServiceImpl implements UserDetailsService {

    private final UserDAO userDao;

    @Autowired
    public AppUserDetailsServiceImpl(UserDAO userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userDao.getUserByEmail(email);
        if (user == null) {
            return null;
        }
        return new AppUserPrincipal(user);
    }
}
