package ru.mail.cops2002.service.models;

import ru.mail.cops2002.repository.models.OrderStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDTO {

    private Integer orderId;
    private OrderStatus status;
    private Integer cost;
    private List<Integer> positionsNamesList = new ArrayList<Integer>();
    private Map<PositionDTO, Integer> positions = new HashMap<PositionDTO, Integer>();
    private Integer userId;
    private UserDTO userDTO;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public List<Integer> getPositionsNamesList() {
        return positionsNamesList;
    }

    public void setPositionsNamesList(List<Integer> positionsNamesList) {
        this.positionsNamesList = positionsNamesList;
    }

    public Map<PositionDTO, Integer> getPositions() {
        return positions;
    }

    public void setPositions(Map<PositionDTO, Integer> positions) {
        this.positions = positions;
    }
}
