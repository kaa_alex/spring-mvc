package ru.mail.cops2002.service.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.repository.models.UserRoles;
import ru.mail.cops2002.repository.models.UserStatus;

import java.util.Collection;
import java.util.Collections;

public class AppUserPrincipal implements UserDetails {

    private User user;
    private Collection<SimpleGrantedAuthority> grantedAuthorities;

    public AppUserPrincipal(User user) {
        this.user = user;
        grantedAuthorities = Collections.singletonList(
                new SimpleGrantedAuthority(user.getRole().name())
        );
    }

    public User getUser() {
        return user;
    }

    public Integer getId() {
        return user.getId();
    }

    public UserRoles getRole() {
        return user.getRole();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {

        if (user.getStatus() == UserStatus.ACTIVE) {
            return true;
        }
        if(user.getStatus() == UserStatus.BLOCKED) {
            return false;
        }
        return true;
    }
}
