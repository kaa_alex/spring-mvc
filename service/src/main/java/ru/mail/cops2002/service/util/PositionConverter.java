package ru.mail.cops2002.service.util;

import ru.mail.cops2002.repository.models.Position;
import ru.mail.cops2002.service.models.PositionDTO;

public class PositionConverter {

    private PositionConverter() {
    }

    public static Position convert(PositionDTO positionDTO) {
        Position position = new Position();
        position.setArticul(positionDTO.getArticul());
        position.setCost(positionDTO.getCost());
        position.setDescription(positionDTO.getDescription());
        return position;
    }

    public static PositionDTO convert(Position position) {
        PositionDTO positionDTO = new PositionDTO();
        positionDTO.setArticul(position.getArticul());
        positionDTO.setCost(position.getCost());
        positionDTO.setDescription(position.getDescription());
        return positionDTO;
    }
}
