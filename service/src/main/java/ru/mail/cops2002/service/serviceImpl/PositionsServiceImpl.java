package ru.mail.cops2002.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.cops2002.repository.OrderDAO;
import ru.mail.cops2002.repository.OrderInfoDAO;
import ru.mail.cops2002.repository.PositionDAO;
import ru.mail.cops2002.repository.models.Order;
import ru.mail.cops2002.repository.models.OrderInfo;
import ru.mail.cops2002.repository.models.OrderStatus;
import ru.mail.cops2002.repository.models.Position;
import ru.mail.cops2002.service.PositionsService;
import ru.mail.cops2002.service.models.PositionDTO;
import ru.mail.cops2002.service.util.PositionConverter;

import java.util.ArrayList;
import java.util.List;

@Service
public class PositionsServiceImpl implements PositionsService {

    private final PositionDAO positionDAO;
    private final OrderInfoDAO orderInfoDAO;
    private final OrderDAO orderDAO;

    @Autowired
    public PositionsServiceImpl(
            PositionDAO positionDAO, OrderInfoDAO orderInfoDAO, OrderDAO orderDAO
    ) {
        this.orderDAO = orderDAO;
        this.positionDAO = positionDAO;
        this.orderInfoDAO = orderInfoDAO;
    }

    @Override
    @Transactional
    public void savePosition(PositionDTO positionDTO) {
        Position position = PositionConverter.convert(positionDTO);
        positionDAO.save(position);
    }

    @Override
    @Transactional
    public List<PositionDTO> getAll() {
        List<Position> positions = positionDAO.getAll();
        List<PositionDTO> positionsDTOList = new ArrayList<>();
        for (Position position : positions) {
            positionsDTOList.add(PositionConverter.convert(position));
        }
        return positionsDTOList;
    }

    @Override
    @Transactional
    public void copyPosition(PositionDTO positionDTO) {

    }

    @Override
    @Transactional
    public PositionDTO getPositionById(Integer articul) {
        PositionDTO positionDTO = null;
        Position position = positionDAO.getById(articul);
        if (position != null) {
            positionDTO = PositionConverter.convert(position);
        }
        return positionDTO;
    }

    @Override
    @Transactional
    public void updatePosition(PositionDTO positionDTO) {
        Position position = PositionConverter.convert(positionDTO);
        positionDAO.update(position);
    }

    @Override
    @Transactional
    public boolean checkPositionEditable(Integer positionArticul) {
        List<OrderInfo> orderInfoList = orderInfoDAO.getOrderInfoByArticul(positionArticul);
        boolean editable = true;
        for (OrderInfo info : orderInfoList) {
            Order order = orderDAO.getById(info.getOrder().getId());
            if (!order.getStatus().equals(OrderStatus.NEW)) {
                editable = false;
                return editable;
            }
        }
        return editable;
    }
}
