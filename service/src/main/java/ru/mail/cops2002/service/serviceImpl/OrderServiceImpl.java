package ru.mail.cops2002.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.cops2002.repository.OrderDAO;
import ru.mail.cops2002.repository.OrderInfoDAO;
import ru.mail.cops2002.repository.models.Order;
import ru.mail.cops2002.service.BasketService;
import ru.mail.cops2002.service.OrderService;
import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.OrderDTO;
import ru.mail.cops2002.service.models.PositionDTO;
import ru.mail.cops2002.service.util.BasketConverter;
import ru.mail.cops2002.service.util.OrderConverter;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderDAO orderDAO;
    private final BasketService basketService;
    private final OrderInfoDAO orderInfoDAO;

    @Autowired
    public OrderServiceImpl(OrderDAO orderDAO, BasketService basketService, OrderInfoDAO orderInfoDAO) {
        this.orderDAO = orderDAO;
        this.basketService = basketService;
        this.orderInfoDAO = orderInfoDAO;
    }

    @Override
    @Transactional
    public void saveOrderByBasket(BasketDTO basketDTO) {
        OrderDTO orderDTO = BasketConverter.convertToOrder(basketDTO);
        Order order = OrderConverter.convert(orderDTO);
        basketService.cleanBasketById(basketDTO.getBasketId());
        orderDAO.save(order);
    }

    @Override
    @Transactional
    public List<OrderDTO> getAll() {
        List<Order> orders = orderDAO.getAll();
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (Order order : orders) {
            orderDTOList.add(OrderConverter.convert(order));
        }
        return orderDTOList;
    }

    @Override
    @Transactional
    public List<OrderDTO> getAllById(Integer id) {
        List<Order> orders = orderDAO.getOrderbyUserId(id);
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (Order order : orders) {
            orderDTOList.add(OrderConverter.convert(order));
        }
        return orderDTOList;
    }

    @Override
    @Transactional
    public OrderDTO getOrderById(Integer id) {
        OrderDTO orderDTO;
        Order order = orderDAO.getById(id);
        if (order == null) {
            orderDTO = null;
        } else {
            orderDTO = OrderConverter.convert(order);
        }
        return orderDTO;
    }

    @Override
    @Transactional
    public void SaveOrUpdate(OrderDTO orderDTO) {
        Order order = OrderConverter.convert(orderDTO);
        orderDAO.saveOrUpdate(order);
    }

    @Override
    @Transactional
    public void deleteFromOrderById(Integer orderId, Integer positionArticul) {
        orderInfoDAO.deleFromOrderById(orderId, positionArticul);
    }

    @Override
    @Transactional
    public void changePositionNumberByArticul(Integer positionArticul, Integer orderId, Integer positionNumber) {
        orderInfoDAO.changePositionNumberById(positionArticul, orderId, positionNumber);
    }

    @Override
    @Transactional
    public Integer getOrderCostByOrderId(Integer id) {
        Order order = orderDAO.getById(id);
        Integer orderCost = 0;
        OrderDTO orderDTO;
        if (order == null) {
            orderDTO = new OrderDTO();
        } else {
            orderDTO = OrderConverter.convert(order);
            if (!orderDTO.getPositions().isEmpty()) {
                for (PositionDTO position : orderDTO.getPositions().keySet()) {
                    Integer positionNumber = orderDTO.getPositions().get(position);
                    orderCost = orderCost + (positionNumber * position.getCost());
                }
            }
            orderDAO.changeOrderCostByOrderId(orderCost, orderDTO.getOrderId());
        }
        return orderCost;
    }

    @Override
    @Transactional
    public void deleteOrderById(Integer id) {
        orderDAO.deleteById(id);
    }
}


