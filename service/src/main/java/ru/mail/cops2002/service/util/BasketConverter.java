package ru.mail.cops2002.service.util;

import ru.mail.cops2002.repository.models.Basket;
import ru.mail.cops2002.repository.models.BasketInfo;
import ru.mail.cops2002.repository.models.OrderStatus;
import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.OrderDTO;
import ru.mail.cops2002.service.models.PositionDTO;

import java.util.Map;

public class BasketConverter {

    private BasketConverter() {
    }

    public static BasketDTO convert(Basket basket) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setBasketCost(basket.getCost());
        basketDTO.setBasketId(basket.getId());
        for (BasketInfo info : basket.getPositions()) {
            PositionDTO position = PositionConverter.convert(info.getPosition());
            basketDTO.getPositions().put(position, info.getNumber());
        }
        basketDTO.setUserId(basket.getUser().getId());
        return basketDTO;
    }

    public static Basket convert(BasketDTO basketDTO) {
        Basket basket = new Basket();
        basket.setCost(basketDTO.getBasketCost());
        basket.setId(basketDTO.getBasketId());
        basket.setUser(UserConverter.convert(basketDTO.getUserDTO()));
        for (Map.Entry<PositionDTO, Integer> position : basketDTO.getPositions().entrySet()) {
            BasketInfo info = new BasketInfo();
            info.setBasket(basket);
            info.setPosition(PositionConverter.convert(position.getKey()));
            info.setNumber(position.getValue());
            basket.getPositions().add(info);
        }
        return basket;
    }

    public static OrderDTO convertToOrder(BasketDTO basketDTO) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setStatus(OrderStatus.NEW);
        orderDTO.setUserId(basketDTO.getUserId());
        orderDTO.setUserDTO(basketDTO.getUserDTO());
        orderDTO.setCost(basketDTO.getBasketCost());
        for (Map.Entry<PositionDTO, Integer> position : basketDTO.getPositions().entrySet()) {
            orderDTO.getPositions().put(position.getKey(), position.getValue());
        }

        return orderDTO;
    }
}
