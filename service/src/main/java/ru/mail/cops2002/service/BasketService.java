package ru.mail.cops2002.service;

import ru.mail.cops2002.service.models.BasketDTO;

public interface BasketService {

    void saveBasket(BasketDTO basketDTO);

    void cleanBasketById(Integer id);

    BasketDTO getBasketById(Integer id);

    BasketDTO getBasketByUserId(Integer id);

    void deleteFromBasketByArticul(Integer articul, Integer basketId);

    Integer getBasketCostByUserId(Integer id);

    void changePositionNumberByArticul(Integer articul, Integer basketId, Integer positionNumber);
}
