package ru.mail.cops2002.service.util;

import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.repository.models.UserInfo;
import ru.mail.cops2002.service.models.UserDTO;


public class UserConverter {

    private UserConverter() {
    }

    public static User convert(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setRole(userDTO.getRole());
        UserInfo userInformation = new UserInfo();
        userInformation.setInformation(userDTO.getInformation());
        userInformation.setAdress(userDTO.getAdress());
        userInformation.setFirstName(userDTO.getFirstName());
        userInformation.setMiddleName(userDTO.getMiddleName());
        userInformation.setLastName(userDTO.getLastName());
        userInformation.setPhone(userDTO.getPhone());
        user.setInfo(userInformation);
        userInformation.setUser(user);
        user.setRole(userDTO.getRole());
        user.setStatus(userDTO.getStatus());
        return user;
    }

    public static UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setInformation(user.getInfo().getInformation());
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        userDTO.setStatus(user.getStatus());
        userDTO.setFirstName(user.getInfo().getFirstName());
        userDTO.setMiddleName(user.getInfo().getMiddleName());
        userDTO.setLastName(user.getInfo().getLastName());
        userDTO.setPhone(user.getInfo().getPhone());
        userDTO.setAdress(user.getInfo().getAdress());
        return userDTO;
    }

}
