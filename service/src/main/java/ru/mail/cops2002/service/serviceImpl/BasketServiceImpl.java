package ru.mail.cops2002.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.cops2002.repository.BasketDAO;
import ru.mail.cops2002.repository.BasketInfoDAO;
import ru.mail.cops2002.repository.models.Basket;
import ru.mail.cops2002.service.BasketService;
import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.PositionDTO;
import ru.mail.cops2002.service.util.BasketConverter;

@Service
public class BasketServiceImpl implements BasketService {

    private final BasketDAO basketDAO;
    private final BasketInfoDAO basketInfoDAO;

    @Autowired
    public BasketServiceImpl(
            BasketDAO basketDAO, BasketInfoDAO basketInfoDAO
    ) {
        this.basketDAO = basketDAO;
        this.basketInfoDAO = basketInfoDAO;
    }

    @Override
    @Transactional
    public void saveBasket(BasketDTO basketDTO) {
        Basket basket = BasketConverter.convert(basketDTO);
        basketDAO.saveOrUpdate(basket);
    }

    @Override
    @Transactional
    public void cleanBasketById(Integer id) {
        basketInfoDAO.cleanBasket(id);
        basketDAO.changeBasketCostByBasketId(0, id);
    }

    @Override
    @Transactional
    public BasketDTO getBasketById(Integer Id) {
        Basket basket = basketDAO.getById(Id);
        BasketDTO basketDTO;
        if (basket == null) {
            basketDTO = new BasketDTO();
        } else {
            basketDTO = BasketConverter.convert(basket);
        }
        return basketDTO;
    }

    @Override
    @Transactional
    public BasketDTO getBasketByUserId(Integer id) {
        Basket basket = basketDAO.getBasketByUserId(id);
        BasketDTO basketDTO;
        if (basket == null) {
            basketDTO = new BasketDTO();
        } else {
            basketDTO = BasketConverter.convert(basket);
        }
        return basketDTO;
    }

    @Override
    @Transactional
    public void deleteFromBasketByArticul(Integer articul, Integer basketId) {
        basketInfoDAO.deleteByArticul(articul, basketId);
    }

    @Override
    @Transactional
    public Integer getBasketCostByUserId(Integer id) {
        Basket basket = basketDAO.getBasketByUserId(id);
        Integer basketCost = 0;
        BasketDTO basketDTO;
        if (basket == null) {
            basketDTO = new BasketDTO();
        } else {
            basketDTO = BasketConverter.convert(basket);
            if (!basketDTO.getPositions().isEmpty()) {
                for (PositionDTO position : basketDTO.getPositions().keySet()) {
                    Integer positionNumber = basketDTO.getPositions().get(position);
                    basketCost = basketCost + (positionNumber * position.getCost());
                }
            }
            basketDAO.changeBasketCostByBasketId(basketCost, basketDTO.getBasketId());
        }
        return basketCost;
    }

    @Override
    @Transactional
    public void changePositionNumberByArticul(Integer articul, Integer basketId, Integer positionNumber) {
        basketInfoDAO.changePositionNumberByArticul(articul, basketId, positionNumber);
    }
}
