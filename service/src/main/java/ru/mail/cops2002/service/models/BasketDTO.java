package ru.mail.cops2002.service.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasketDTO {

    private UserDTO userDTO;
    private Integer UserId;
    private Integer BasketId;
    private List<Integer> positionsNamesList = new ArrayList<Integer>();
    private Map<PositionDTO, Integer> positions = new HashMap<PositionDTO, Integer>();
    private Integer BasketCost = 0;

    public BasketDTO() {
        BasketId = null;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public Integer getBasketId() {
        return BasketId;
    }

    public void setBasketId(Integer basketId) {
        BasketId = basketId;
    }

    public Map<PositionDTO, Integer> getPositions() {
        return positions;
    }

    public Integer getBasketCost() {
        return BasketCost;
    }

    public void setBasketCost(Integer basketCost) {
        BasketCost = basketCost;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public List<Integer> getPositionsNamesList() {
        return positionsNamesList;
    }

    public void setPositionsNamesList(List<Integer> positionsNamesList) {
        this.positionsNamesList = positionsNamesList;
    }

    public void setPositions(Map<PositionDTO, Integer> positions) {
        this.positions = positions;
    }
}
