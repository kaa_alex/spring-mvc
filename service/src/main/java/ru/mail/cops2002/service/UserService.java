package ru.mail.cops2002.service;


import ru.mail.cops2002.service.models.UserDTO;

import java.util.List;

public interface UserService {

    void saveUser(UserDTO userDTO);

    List<UserDTO> getAll();

    UserDTO getUserById(Integer id);

    void saveOrUpdateUser(UserDTO userDTO);

    void saveUserChanges(UserDTO userDTO);

    void changePassword(UserDTO userDTO, String password);

    void saveUserInfoChanges(UserDTO userDTO);

    void updateUserPassword(UserDTO userDTO, String password);
}
