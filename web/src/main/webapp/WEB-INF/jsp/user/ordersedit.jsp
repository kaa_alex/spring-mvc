<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="utf-8">
    <title>Waterdelivery: Order Edit</title>
</head>
<body>
<div style="float: right">
    <form:form method="post" action="../logout">
        <input type="submit" value="Logout">
    </form:form>
</div>
<center>
    <p>
    <p><img src="../files/logo.jpg" alt="water delivery"></p>
    </p>
    <p align="center">Order ${orderId}: </p>
    <div style="align-content: center; align-self: center">
        <form:form commandName="order" action="orderaction" method="post">
            <form:input path="orderId" text="${orderId}" hidden="true"/>
            <c:forEach items="${positions}" var="position">
                <p><form:checkbox path="positionsNamesList"
                                  label="${position.key.description}, cost : ${position.key.cost}, number in basket :"
                                  value="${position.key.articul}"></form:checkbox>
                    <input type="number" name="${position.key.articul}"
                           value="${position.value}">
                </p>
            </c:forEach>
            <p>Total cost :
                <input type=" text" name="ordercost" contenteditable="false" value="${ordercost}"
                       readonly="readonly">
            </p>
            <p>
                <input type="submit" name="action" value="Delete positions">
            </p>
            <p>
                <input type="submit" name="action" value="Save Order">
            </p>
        </form:form>
    </div>
    <form:form method="get" action="orders">
        <p>
            <input type="submit" value="Back">
        </p>
    </form:form>
</center>
</body>
</html>
