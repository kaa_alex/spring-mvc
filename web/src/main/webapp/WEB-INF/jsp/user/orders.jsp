<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<head>
    <title>Orders List</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <c:if test="${isEmpty == true}">
        Your orders is empty, please, add positions.
        <form:form method="get" action="catalog">
            <p>
                <input type="submit" value="Catalog">
            </p>
        </form:form>
    </c:if>
    <c:if test="${isEmpty == false}">
        <table>
            <thead>
            <tr>
                <th>Order Id</th>
                <th>Order Cost</th>
                <th>Order Status</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="order" items="${orders}">
                <tr>
                    <th><c:out value="${order.orderId}"/></th>
                    <td><c:out value="${order.cost}"/></td>
                    <td><c:out value="${order.status}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <form:form method="post" modelAttribute="order" action="editorder">
            <p>
                <form:errors path="orderId"/>
            </p>
            <c:if test="${param['error']}">
                <p>We don't find order with this id, please enter another number</p>
            </c:if>
            <c:if test="${param['noneditable']}">
                <p>
                    You can edit orders only with status "NEW"!
                </p>
            </c:if>
            <b>Enter order id for edit : </b>
            <br>
            <form:input path="orderId" size="5" type="number" required="true"/> <input type="submit" value="edit">
            <p></p>
        </form:form>
    </c:if>
    <form:form method="get" action="start">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</center>
</body>