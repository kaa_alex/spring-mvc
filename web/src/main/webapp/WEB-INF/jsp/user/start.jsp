<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery : User registration</title>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p><img src="../files/logo.jpg" alt="water delivery"></p>
    </p>
    <p>Hello user!</p>
    <form:form method="get" action="news">
        <p>
            <input type="submit" value="NEWS">
        </p>
    </form:form>
    <form:form method="get" action="basket">
        <p>
            <input type="submit" value="Basket">
        </p>
    </form:form>
    <form:form method="get" action="orders">
        <p>
            <input type="submit" value="Orders">
        </p>
    </form:form>
    <form:form method="get" action="catalog">
        <p>
            <input type="submit" value="Catalog">
        </p>
    </form:form>
    <form:form method="get" action="edituser">
        <p>
            <input type="submit" value="Change information">
        </p>
    </form:form>
</center>
</body>
</html>