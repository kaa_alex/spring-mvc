<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="utf-8">
    <title>Waterdelivery: Catalog</title>
</head>
<body>
<div style="float: right">
    <form:form method="get" action="basket">
        <input type="submit" value="Basket">
    </form:form>
    <form:form method="post" action="../logout">
        <input type="submit" value="Logout">
    </form:form>
</div>
<center>
    <p>
    <p><img src="../files/logo.jpg" alt="water delivery"></p>
    </p>
    <p align="center">Catalog : </p>
    <div style="align-content: center; align-self: center">
        <c:forEach items="${positions}" var="positionDTO">
        <form:form method="post" modelAttribute="position" action="basketadd">
            <form:input path="articul" value="${positionDTO.key.articul}" readonly="true" hidden="true"/>
            <form:input path="description" value="${positionDTO.key.description}" readonly="true"/>
            <form:input path="cost" value="${positionDTO.key.cost}" readonly="true"/>
            <input type="text" name="basketcost" contenteditable="false" value="${basketcost}" readonly="readonly" hidden>
            <input name="number" value="${positionDTO.value}" type="number">
        <c:if test="${positionDTO.value > 0}">
        This position allready in basket.
        <input type="submit" name="submit" value="delete from basket">
        </c:if>
        <c:if test="${positionDTO.value < 1}">
        <input type="submit" name="submit" value="add to basket">
        </c:if>
        </form:form>
        </c:forEach>
            <p>Total cost :
            <input type="text" contenteditable="false" value="${basketcost}" readonly="readonly">
            </p>
        <form:form method="get" action="start">
        <p>
            <input type="submit" value="Back">
        </p>
        </form:form>
</center>
</body>
</html>
