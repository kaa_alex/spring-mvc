<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery : User registration</title>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p><img src="../files/logo.jpg" alt="water delivery"></p>
    </p>
    <p>Hello user!</p>
    <form:form method="post" modelAttribute="user" action="saveuserchanges">
        <p>
            <form:errors path="email"/>
        </p>
        <form:input path="id" value="${user.id}" hidden="true"/>
        <c:if test="${passworderror}">
            <p>Password repeated wrong</p>
        </c:if>
        <b>Enter new password :</b>
        <br>
        <form:input path="password" type="password" size="50" placeholder="Enter new password"/>
        <p></p>
        <b>Repeat new password</b><br>
        <input name="passwordrepeat" type="password" size="50" placeholder="Repeat password"/>
        <p></p>
        <b>Enter your first name :</b>
        <br>
        <form:input path="firstName" type="text" size="50" required="true" value="${user.firstName}"/>
        <p></p>
        <b>Enter your middle name :</b>
        <br>
        <form:input path="middleName" type="text" size="50" required="true" value="${user.middleName}"/>
        <p></p>
        <b>Enter your last name :</b>
        <br>
        <form:input path="lastName" type="text" size="50" required="true" value="${user.lastName}"/>
        <p></p>
        <b>Enter your phone :</b>
        <br>
        <form:input path="phone" type="text" size="50" required="true" value="${user.phone}"/>
        <p></p>
        <b>Enter your adress :</b>
        <br>
        <form:input path="adress" type="text" size="50" required="true" value="${user.adress}"/>
        <p></p>
        <b>Enter information :</b>
        <br>
        <form:textarea path="information" size="255" required="true"/>
        <p></p>
        <p>
            <input type="submit" value="Save changes">
        </p>
    </form:form>
    <form:form action="start" method="get">
        <p>
            <input type="submit" value="Back">
        </p>
    </form:form>
</center>
</body>
</html>