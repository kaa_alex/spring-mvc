<!DOCTYPE html >
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery</title>
</head>
<body>
<center>
    <p><img src="files/logo.jpg" alt="water delivery"></p>
    <form action="login" method='POST'>
        <c:if test="${param['error']}">
            <p>Email or password is not valid, please try again</p>
        </c:if>
        <b>Enter your email :</b><br>
        <input type="text" id="email" name="email" placeholder="Email" required>
        <p></p>
        <b>Enter your password :</b><br>
        <input type="password" id="password" name="password" placeholder="Password" required>
        <p><input type="submit" value="Sign In">
        </p>
    </form>
    <form name="Registration" method="get" action="registration">
        <input type="submit" value="Registration">
    </form>
</center>
</body>
</html>