<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: ykypok
  Date: 01.10.2017
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WaterDelivery: NEWS</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right">
    <form:form method="post" action="../logout">
        <input type="submit" value="Logout">
    </form:form>
</div>
<CENTER>
    <div>
        <p>
        <p><img src="../files/logo.jpg" alt="water delivery"></p>
        </p>
        <c:if test="${isEmpty==true}">
            <p>NEWS is empty!</p>
        </c:if>
        <c:if test="${isEmpty==false}">
            <table>
                <thead>
                <tr>
                    <th>News ID</th>
                    <th>News Header</th>
                    <th>News Picture</th>
                    <th>News Content</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${newslist}">
                    <tr>
                        <th><c:out value="${news.newsId}"/></th>
                        <th><c:out value="${news.header}"/></th>
                        <td><img src="../files/${news.fileName}" height="42" width="42"></td>
                        <td><c:out value="${news.content}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <form:form method="post" modelAttribute="news" action="editnews">
                <p>
                    <form:errors path="newsId"/>
                </p>
                <c:if test="${param['error']}">
                    <p>We don't find news with this id, please enter another number</p>
                </c:if>
                <b>Enter news id for edit : </b>
                <br>
                <form:input path="newsId" size="5" type="number" required="true"/>
                <p><input type="submit" name="submit" value="edit"></p>
                <p><input type="submit" name="submit" value="delete"></p>
                <p></p>
            </form:form>
        </c:if>
    </div>
    <form:form method="get" action="addnews">
        <input type="submit" value="add News"/>
    </form:form>
    <form:form method="get" action="start">
        <input type="submit" value="back"/>
    </form:form>
</center>
</body>
</html>
