<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<head>
    <title>Users List</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <table>
        <thead>
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Role</th>
            <th>Status</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Phone</th>
            <th>Adress</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${users}">
            <tr>
                <th><c:out value="${user.id}"/></th>
                <td><c:out value="${user.email}"/></td>
                <td><c:out value="${user.role}"/></td>
                <td><c:out value="${user.status}"/></td>
                <td><c:out value="${user.firstName}"/></td>
                <td><c:out value="${user.middleName}"/></td>
                <td><c:out value="${user.lastName}"/></td>
                <td><c:out value="${user.phone}"/></td>
                <td><c:out value="${user.adress}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <form:form method="post" modelAttribute="user" action="edituser">
        <p>
            <form:errors path="id"/>
        </p>
        <c:if test="${param['error']}">
            <p>We don't find user with this id, please enter another number</p>
        </c:if>
        <b>Enter user id for edit : </b>
        <br>
        <form:input path="id"  size="5" type="number" required="true"/> <input type="submit" value="edit">
        <p></p>
    </form:form>
    <form:form method="get" action="start">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</center>
</body>