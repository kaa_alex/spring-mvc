<%--
  Created by IntelliJ IDEA.
  User: ykypok
  Date: 31.08.2017
  Time: 15:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<head>
    <title>Edit Order</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <table>
        <thead>
        <tr>
            <th>User Id</th>
            <th>Order Id</th>
            <th>Order Cost</th>
            <th>Order Status</th>
        </tr>
        </thead>
        <form:form method="post" modelAttribute="order" action="saveorderchanges">
        <tbody>
        <tr>
            <th><c:out value="${order.userId}"/></th>
            <th><c:out value="${order.orderId}"/></th>
            <td><c:out value="${order.cost}"/></td>
            <td><form:select path="status" items="${status}"/></td>
        </tr>
        </tbody>
    </table>
    <form:input path="orderId" text="${order.orderId}" hidden="true"/>
    <form:input path="userId" text="${order.userId}" hidden="true"/>
    <p>
        <input type="Submit" name="submit" value="Save Changes">
    </p>
    </form:form>
    <form:form method="get" action="orders">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</center>
</body>
</html>
