<%--
  Created by IntelliJ IDEA.
  User: ykypok
  Date: 31.08.2017
  Time: 15:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<head>
    <title>Edit User</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <table>
        <thead>
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Role</th>
            <th>Status</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Phone</th>
            <th>Address</th>
        </tr>
        </thead>
        <form:form method="post" modelAttribute="user" action="savechanges">
        <tbody>
        <tr>
            <td><c:out value="${user.id}"/></td>
            <td><c:out value="${user.email}"/></td>
            <td><form:select path="role" items="${roles}"/></td>
            <td><form:select path="status" items="${status}"/></td>
            <td><c:out value="${user.firstName}"/></td>
            <td><c:out value="${user.middleName}"/></td>
            <td><c:out value="${user.lastName}"/></td>
            <td><c:out value="${user.phone}"/></td>
            <td><c:out value="${user.adress}"/></td>
        </tr>
        </tbody>
    </table>
    <form:input path="id" text="${user.id}" hidden="true"/>
    <p>
        Enter new password for this user : <input name="password" type="password" placeholder="Enter New password"
                                                  size="15"/>
    </p>
    <p>
        <input type="Submit" name="submit" value="Save Changes">
    </p>
    </form:form>
    <form:form method="get" action="users">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</center>
</body>
</html>
