<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html >
<head>
    <title>Users List</title>
    <style type="text/css">
        BODY {
            background: white;
        }

        TABLE {
            width: 300px;
            border-collapse: collapse;
            border: 2px solid maroon;
        }

        TD, TH {
            padding: 3px;
            border: 1px solid maroon;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <table>
        <caption>Price :</caption>
        <thead>
        <tr>
            <th>Arcticul</th>
            <th>Description</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="position" items="${positions}">
            <tr>
                <th><c:out value="${position.articul}"/></th>
                <td><c:out value="${position.description}"/></td>
                <td><c:out value="${position.cost}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <form:form method="post" modelAttribute="position" action="positionedit">
        <p>
            <form:errors path="articul"/>
        </p>
        <c:if test="${param['error']}">
            <p>We don't find position with this articul, please enter another number</p>
        </c:if>
        <c:if test="${param['noneditable']}">
            <p>This position allready in orders, you can't edit it</p>
        </c:if>
        <p align="center">
            Input number of position for edit or copy: <form:input size="5" path="articul" type="number"
                                                                   required="true"/>
        </p>
        <p><input type="Submit" name="submit" value="Copy Position"/></p>
        <p><input type="Submit" name="submit" value="Edit Position"/></p>
    </form:form>
    <form:form method="post" action="newposition">
        <p><input type="Submit" value="Create New Position"/></p>
    </form:form>
    <form:form method="get" action="start">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</center>
</body>