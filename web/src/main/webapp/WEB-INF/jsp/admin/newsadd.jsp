<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ykypok
  Date: 01.10.2017
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WaterDelivery: Add news</title>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<center>
    <p>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    <form:form modelAttribute="news" method="POST" action="upload" enctype="multipart/form-data">
        <c:if test="${param['fileisempty']}">
            Please choose file for thie news!
        </c:if>
        <div>
            <p>
                <form:input id="file" path="file" type="file"/>
            </p>
        </div>
        <p>Input header : </p>
        <p><form:input path="header" type="text" required="true"/></p>
        <p>Input content : </p>
        <p><form:input id="content" path="content" type="text" required="true"/></p>
        <p><input type="submit" value="Save"/></p>
    </form:form>
    <form:form method="get" action="news">
        <p><input type="submit" value="Back"/></p>
    </form:form>
</center>
</body>
</html>
