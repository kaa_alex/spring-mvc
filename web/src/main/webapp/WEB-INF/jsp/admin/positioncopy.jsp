<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery : Superadministrator room</title>
</head>
<body>
<center>
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    <form:form method="post" modelAttribute="position"  action="savenewposition">
        <p> Position description : <form:input type="text" path="description" value="${position.description}"/></p>
        <p>Position cost : <form:input pattern="^[ 0-9]+$" path="cost" value="${position.cost}"/></p>
        <p><input type="submit" value="Save"></p>
    </form:form>
    <form:form method="get" action="positions">
        <p><input type="submit" value="Back"></p>
    </form:form>
</center>
</body>
</html>