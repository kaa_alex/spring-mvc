<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery : Superadministrator room</title>
</head>
<body>
<div style="float: right"><form:form method="post" action="../logout">
    <input type="submit" value="Logout">
</form:form>
</div>
<div align="center">
    <p>
        <img src="../files/logo.jpg" alt="water delivery">
    </p>
    <form:form method="post" modelAttribute="position" action="savenewposition">
        <p align="center">
            <b>Enter position description :</b>
        </p>
        <form:input path="description" type="text" size="50" required="true"/>
        <p></p>
        <p>
        <p align="center">
            <b>Enter position cost :</b>
        </p>
        <form:input path="cost" pattern="^[ 0-9]+$" size="50" required="true"/>
        <p></p>
        <p>
            <input type="submit" value="Save position">
        </p>
    </form:form>
    <form:form method="get" action="positions">
        <p><input type="Submit" value="Back"/></p>
    </form:form>
</div>
</body>
</html>