<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Drinking Water Delivery : User registration</title>
</head>
<body>
<center>
    <p>
    <p>
        <img src="files/logo.jpg" alt="water delivery">
    </p>
    </p>
    <form:form method="post" modelAttribute="user" action="registration">
        <p>
            <form:errors path="email"/>
        </p>
        <b>Enter your email :</b>
        <br>
        <form:input path="email" type="email" size="50"/>
        <p></p>
        <b>Enter your password :</b>
        <br>
        <form:input path="password" type="password" size="50"/>
        <p></p>
        <c:if test="${passworderror}">
            <p>Password repeated wrong</p>
        </c:if>
        <p></p>
        <b>Repeat password</b>
        <br>
        <input name="passwordrepeat" type="password" size="50"/>
        <p></p>
        <b>Enter your first name :</b>
        <br>
        <form:input path="firstName" type="text" size="50" required="true"/>
        <p></p>
        <b>Enter your middle name :</b>
        <br>
        <form:input path="middleName" type="text" size="50" required="true"/>
        <p></p>
        <b>Enter your last name :</b>
        <br>
        <form:input path="lastName" type="text" size="50" required="true"/>
        <p></p>
        <b>Enter your phone :</b>
        <br>
        <form:input path="phone" type="text" size="50" required="true"/>
        <p></p>
        <b>Enter your adress :</b>
        <br>
        <form:input path="adress" type="text" size="50" required="true"/>
        <p></p>
        <b>Enter information :</b>
        <br>
        <form:textarea path="information" type="text" size="255" required="true"/>
        <p></p>
        <p>
            <input type="submit" value="Complete registration">
        </p>
    </form:form>
    <form:form action="back">
        <p>
            <input type="submit" value="Back">
        </p>
    </form:form>
</center>
</body>
</html>