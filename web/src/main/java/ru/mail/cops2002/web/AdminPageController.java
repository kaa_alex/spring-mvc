package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.service.NewsService;
import ru.mail.cops2002.service.OrderService;
import ru.mail.cops2002.service.PositionsService;
import ru.mail.cops2002.service.models.NewsDTO;
import ru.mail.cops2002.service.models.OrderDTO;
import ru.mail.cops2002.service.models.PositionDTO;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AdminPageController {

    private final PositionsService positionsService;
    private final OrderService orderService;
    private final NewsService newsService;

    @Autowired
    public AdminPageController(PositionsService positionsService, OrderService orderService, NewsService newsService) {
        this.orderService = orderService;
        this.positionsService = positionsService;
        this.newsService = newsService;
    }

    @RequestMapping(value = "/positions", method = RequestMethod.GET)
    public java.lang.String getAllPositions(Model model) {
        List<PositionDTO> positions = positionsService.getAll();
        model.addAttribute("positions", positions);
        model.addAttribute("position", new PositionDTO());
        return "admin/positions";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String getAllOrders(Model model) {
        boolean isEmpty = false;
        List<OrderDTO> orders = orderService.getAll();
        if (orders.isEmpty()) {
            isEmpty = true;
        }
        model.addAttribute("isEmpty", isEmpty);
        model.addAttribute("orders", orders);
        model.addAttribute("order", new OrderDTO());
        return "admin/orders";
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String getAllNews(Model model) {
        boolean isEmpty = false;
        List<NewsDTO> newsList = newsService.getAll();
        if (newsList.isEmpty()) {
            isEmpty = true;
        }
        model.addAttribute("isEmpty", isEmpty);
        model.addAttribute("newslist", newsList);
        model.addAttribute("news", new NewsDTO());
        return "admin/news";
    }
}
