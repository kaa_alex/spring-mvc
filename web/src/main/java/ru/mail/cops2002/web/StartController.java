package ru.mail.cops2002.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StartController {

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String showWelcomePage() {
		return "login";
	}

}
