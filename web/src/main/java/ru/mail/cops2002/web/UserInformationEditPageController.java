package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mail.cops2002.service.UserService;
import ru.mail.cops2002.service.models.UserDTO;

@Controller
@RequestMapping(value = "/user")
public class UserInformationEditPageController {

    private final UserService userService;

    @Autowired
    public UserInformationEditPageController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "saveuserchanges", method = RequestMethod.POST)
    public String saveUserChanges(@ModelAttribute("user") UserDTO userDTO, Model model, @RequestParam String passwordrepeat) {
        if (userDTO.getPassword() != ("")) {
            if (passwordrepeat.equals(userDTO.getPassword())) {
                userService.updateUserPassword(userDTO, userDTO.getPassword());
            } else {
                model.addAttribute("passworderror", true);
                model.addAttribute("user", userDTO);
                return "user/infoedit";
            }
        }
        userService.saveUserInfoChanges(userDTO);
        return "user/start";
    }
}
