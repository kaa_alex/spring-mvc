package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.repository.models.OrderStatus;
import ru.mail.cops2002.service.OrderService;
import ru.mail.cops2002.service.models.OrderDTO;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AdminOrdersPageController {

    private final OrderService orderService;

    @Autowired
    public AdminOrdersPageController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "/editorder", method = RequestMethod.POST)
    public String EditOrder(@ModelAttribute("order") OrderDTO order, Model model) {
        OrderDTO orderDTO = orderService.getOrderById(order.getOrderId());
        List<OrderStatus> list = new ArrayList<>();
        list.add(OrderStatus.NEW);
        list.add(OrderStatus.REVIEWING);
        list.add(OrderStatus.IN_PROGRESS);
        list.add(OrderStatus.DELIVERED);
        if (orderDTO != null) {
            model.addAttribute("status", list);
            model.addAttribute("order", orderDTO);
            return "admin/ordersedit";
        } else {
            model.addAttribute("error", true);
            return "redirect:orders";
        }
    }

    @RequestMapping(value = "/saveorderchanges", method = RequestMethod.POST)
    public String actionChoice(@ModelAttribute("order") OrderDTO order) {
        OrderDTO orderDTO = orderService.getOrderById(order.getOrderId());
        orderDTO.setStatus(order.getStatus());
        orderService.SaveOrUpdate(orderDTO);
        return "redirect:orders";
    }
}
