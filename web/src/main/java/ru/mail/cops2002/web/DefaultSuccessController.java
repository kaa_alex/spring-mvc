package ru.mail.cops2002.web;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.repository.models.UserRoles;
import ru.mail.cops2002.service.models.AppUserPrincipal;


@Controller
public class DefaultSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal.getRole() == UserRoles.ROLE_USER) {
            return "redirect:/user/start";
        } else if (principal.getRole() == UserRoles.ROLE_ADMIN) {
            return "redirect:/admin/start";
        } else if (principal.getRole() == UserRoles.ROLE_SUPERADMIN)
            return "redirect:/superadmin/start";
        else
            return "redirect:/login";
    }
}
