package ru.mail.cops2002.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.service.*;
import ru.mail.cops2002.service.models.*;

import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserPageController {

    private final PositionsService positionsService;
    private final BasketService basketService;
    private final OrderService orderService;
    private final UserService userService;
    private final NewsService newsService;


    @Autowired
    public UserPageController(BasketService basketService, PositionsService positionsService, OrderService orderService,
                              UserService userService, NewsService newsService) {
        this.positionsService = positionsService;
        this.basketService = basketService;
        this.orderService = orderService;
        this.userService = userService;
        this.newsService = newsService;
    }

    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String catalog(Model model) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        List<PositionDTO> positions = positionsService.getAll();
        BasketDTO basketDTO = basketService.getBasketByUserId(user.getId());
        for (PositionDTO position : positions) {
            if (!basketDTO.getPositions().containsKey(position)) {
                basketDTO.getPositions().put(position, 0);
            }
        }
        basketDTO.setBasketCost(basketService.getBasketCostByUserId(user.getId()));
        model.addAttribute("basketcost", basketDTO.getBasketCost());
        model.addAttribute("positions", basketDTO.getPositions());
        model.addAttribute("position", new PositionDTO());
        return "user/catalog";
    }

    @RequestMapping(value = "/basket", method = RequestMethod.GET)
    public String basket(Model model) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        boolean isEmpty = true;
        BasketDTO basketDTO = basketService.getBasketByUserId(user.getId());
        if (!basketDTO.getPositions().isEmpty()) {
            isEmpty = false;
            for (PositionDTO position : basketDTO.getPositions().keySet()) {
                basketDTO.getPositionsNamesList().add(position.getArticul());
            }
        }
        basketDTO.setBasketCost(basketService.getBasketCostByUserId(user.getId()));
        model.addAttribute("isEmpty", isEmpty);
        model.addAttribute("basketcost", basketDTO.getBasketCost());
        model.addAttribute("positions", basketDTO.getPositions());
        model.addAttribute("basket", new BasketDTO());
        return "user/basket";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public java.lang.String getOrders(Model model) {
        boolean isEmpty = false;
        AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        List<OrderDTO> orders = orderService.getAllById(user.getId());
        if (orders.isEmpty()) {
            isEmpty = true;
        }
        model.addAttribute("isEmpty", isEmpty);
        model.addAttribute("orders", orders);
        model.addAttribute("order", new OrderDTO());
        return "user/orders";
    }

    @RequestMapping(value = "/edituser", method = RequestMethod.GET)
    public String editInfo(Model model) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        UserDTO userDTO = userService.getUserById(user.getId());
        userDTO.setPassword("");
        model.addAttribute("user", userDTO);
        return "user/infoedit";
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String getAllNews(Model model) {
        boolean isEmpty = false;
        List<NewsDTO> newsList = newsService.getAll();
        if (newsList.isEmpty()) {
            isEmpty = true;
        }
        model.addAttribute("isEmpty", isEmpty);
        model.addAttribute("newslist", newsList);
        model.addAttribute("news", new NewsDTO());
        return "user/news";
    }
}

