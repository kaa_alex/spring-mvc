package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.service.NewsService;
import ru.mail.cops2002.service.models.NewsDTO;

import javax.servlet.ServletContext;
import java.io.IOException;

@Controller
@RequestMapping("/admin")
public class AdminNewsEditPageController {

    private final NewsService newsService;
    private final ServletContext context;

    @Autowired
    public AdminNewsEditPageController(NewsService newsService, ServletContext servletContext) {
        this.newsService = newsService;
        this.context = servletContext;
    }

    @RequestMapping(value = "/updatenews", method = RequestMethod.POST)
    public String updateNews(@ModelAttribute("news") NewsDTO newsDTO, BindingResult result, Model model) throws IOException {
        if (newsDTO.getFile().isEmpty()) {
            newsService.updateWithoutPicture(newsDTO);
        } else {
            String relativeWebPath = "/files/";
            String absoluteFilePath = context.getRealPath(relativeWebPath);
            if (!result.hasErrors()) {
                newsService.update(newsDTO, absoluteFilePath);
            }
        }
        return "redirect: news";
    }
}
