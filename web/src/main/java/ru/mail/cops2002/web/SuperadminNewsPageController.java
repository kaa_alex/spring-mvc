package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.service.NewsService;
import ru.mail.cops2002.service.models.NewsDTO;

import java.io.IOException;

@Controller
@RequestMapping("/superadmin")
public class SuperadminNewsPageController {


    private final NewsService newsService;

    @Autowired
    public SuperadminNewsPageController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = "/addnews", method = RequestMethod.GET)
    public String addNews(Model model) {
        model.addAttribute("news", new NewsDTO());
        return "superadmin/newsadd";
    }

    @RequestMapping(value = "/editnews", method = RequestMethod.POST)
    public String editNews(@ModelAttribute("news") NewsDTO newsDTO, Model model, String submit) throws IOException {
        NewsDTO news = newsService.getById(newsDTO.getNewsId());
        if (news == null) {
            model.addAttribute("error", true);
        } else {
            if (submit.equals("edit")) {
                model.addAttribute("news", news);
                return "superadmin/newsedit";
            }
            if (submit.equals("delete")) {
                newsService.delete(newsDTO.getNewsId());
            }
        }
        return "redirect: news";
    }


}
