package ru.mail.cops2002.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WorkingStartController {

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/user/start", method = RequestMethod.GET)
    public String showUserWelcomePage() {
        return "user/start";
    }

    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
    @RequestMapping(value = "/superadmin/start", method = RequestMethod.GET)
    public String showSuperAdminWelcomePage() {
        return "superadmin/start";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/start", method = RequestMethod.GET)
    public String showAdminWelcomePage() {
        return "admin/start";
    }
}
