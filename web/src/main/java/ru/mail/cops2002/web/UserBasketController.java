package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.service.BasketService;
import ru.mail.cops2002.service.OrderService;
import ru.mail.cops2002.service.models.AppUserPrincipal;
import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.PositionDTO;
import ru.mail.cops2002.service.util.UserConverter;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Controller
@RequestMapping(value = "/user")
public class UserBasketController {

    private final BasketService basketService;
    private final OrderService orderService;

    @Autowired
    public UserBasketController(BasketService basketService, OrderService orderService) {
        this.basketService = basketService;
        this.orderService = orderService;
    }

    @RequestMapping(value = "/basketaction", method = RequestMethod.POST)
    public String basketaction(Model model, BasketDTO basket, String action, HttpServletRequest request) {
        AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        BasketDTO basketDTO = basketService.getBasketByUserId(user.getId());
        Integer basketId = basketDTO.getBasketId();
        if (action.equals("Delete positions")) {
            if (basket.getPositionsNamesList().isEmpty()) {
            } else {
                for (Integer articul : basket.getPositionsNamesList()) {
                    basketService.deleteFromBasketByArticul(articul, basketId);
                }
            }
        } else {
            for (PositionDTO position : basketDTO.getPositions().keySet()) {
                String positionNumber = request.getParameter(position.getArticul().toString());
                Integer number = Integer.parseInt(positionNumber);
                if (number > 0) {
                    if (!Objects.equals(basketDTO.getPositions().get(position), number)) {
                        basketService.changePositionNumberByArticul(position.getArticul(), basketId, number);
                    }
                }
            }
            basketDTO.setBasketCost(basketService.getBasketCostByUserId(user.getId()));
            if (action.equals("Create Order")) {
                basketDTO = basketService.getBasketByUserId(user.getId());
                basketDTO.setUserDTO(UserConverter.convert(user));
                orderService.saveOrderByBasket(basketDTO);
            }

        }
        basketDTO.setBasketCost(basketService.getBasketCostByUserId(user.getId()));
        model.addAttribute("basket", basket);
        model.addAttribute("positionsArticul", basketDTO.getPositionsNamesList());
        return "redirect: basket";
    }
}
