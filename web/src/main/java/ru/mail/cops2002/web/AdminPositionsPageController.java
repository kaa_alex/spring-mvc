package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mail.cops2002.service.PositionsService;
import ru.mail.cops2002.service.models.PositionDTO;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AdminPositionsPageController {

    private final PositionsService positionsService;

    @Autowired
    public AdminPositionsPageController(PositionsService positionsService) {
        this.positionsService = positionsService;
    }

    @RequestMapping(value = "/newposition", method = RequestMethod.POST)
    public java.lang.String createNewPosition(Model model) {
        model.addAttribute("position", new PositionDTO());
        return "admin/newposition";
    }

    @RequestMapping(value = "/savenewposition", method = RequestMethod.POST)
    public String saveNewPosition(@ModelAttribute("position") PositionDTO position, Model model) {
        positionsService.savePosition(position);
        List<PositionDTO> positions = positionsService.getAll();
        model.addAttribute("positions", positions);
        model.addAttribute("position", new PositionDTO());
        return "admin/positions";
    }

    @RequestMapping(value = "/positionedit", method = RequestMethod.POST)
    public String editPosition(@RequestParam String submit, PositionDTO position, Model model) {
        PositionDTO positionDTO = positionsService.getPositionById(position.getArticul());
        if (positionDTO != null) {
            model.addAttribute("position", positionDTO);
            if (submit.equals("Copy Position")) {
                return "admin/positioncopy";
            }
            if (submit.equals("Edit Position")) {
                boolean editable = positionsService.checkPositionEditable(positionDTO.getArticul());
                if (editable) {
                    return "admin/positionedit";
                } else {
                    model.addAttribute("noneditable", true);
                    return "redirect:positions";
                }
            }
        }
        model.addAttribute("error", true);
        return "redirect:positions";
    }

    @RequestMapping(value = "/positionsavechanges", method = RequestMethod.POST)
    public String savePositionChanges(@ModelAttribute("position") PositionDTO position, Model model) {
        positionsService.updatePosition(position);
        List<PositionDTO> positions = positionsService.getAll();
        model.addAttribute("positions", positions);
        model.addAttribute("position", new PositionDTO());
        return "admin/positions";
    }
}
