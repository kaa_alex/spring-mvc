package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mail.cops2002.repository.models.UserRoles;
import ru.mail.cops2002.repository.models.UserStatus;
import ru.mail.cops2002.service.UserService;
import ru.mail.cops2002.service.models.UserDTO;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(value = "/superadmin")
public class SuperadminUsersEditPageController {

    private final UserService userService;

    @Autowired
    public SuperadminUsersEditPageController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/edituser", method = RequestMethod.POST)
    public String EditUser(@ModelAttribute("user") UserDTO user, Model model) {
        UserDTO userDTO = userService.getUserById(user.getId());
        List<UserRoles> list = new ArrayList<>();
        list.add(UserRoles.ROLE_USER);
        list.add(UserRoles.ROLE_ADMIN);
        list.add(UserRoles.ROLE_SUPERADMIN);
        List<UserStatus> statusList = new ArrayList<>();
        statusList.add(UserStatus.ACTIVE);
        statusList.add(UserStatus.BLOCKED);
        if (userDTO != null) {
            model.addAttribute("status", statusList);
            model.addAttribute("roles", list);
            model.addAttribute("user", userDTO);
            return "superadmin/useredit";
        } else {
            model.addAttribute("error", true);
            return "redirect:users";
        }
    }

    @RequestMapping(value = "/savechanges", method = RequestMethod.POST)
    public String actionChoice(@RequestParam String password, UserDTO user, Model model) {
        if (password.equals("")) {
            userService.saveUserChanges(user);
        } else {
            userService.changePassword(user, password);
        }
        List<UserDTO> users = userService.getAll();
        model.addAttribute("users", users);
        model.addAttribute("user", new UserDTO());
        return "superadmin/users";
    }
}

