package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.repository.models.OrderStatus;
import ru.mail.cops2002.service.OrderService;
import ru.mail.cops2002.service.models.OrderDTO;
import ru.mail.cops2002.service.models.PositionDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Controller
@RequestMapping(value = "/user")
public class UserOrderEditPage {

    private final OrderService orderService;

    @Autowired
    public UserOrderEditPage(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "/editorder", method = RequestMethod.POST)
    public String actionChoice(@ModelAttribute("order") OrderDTO order, Model model) {
        OrderDTO orderDTO = orderService.getOrderById(order.getOrderId());
        if (orderDTO == null) {
            model.addAttribute("error", true);
            return "redirect:orders";
        } else {
            if (!orderDTO.getStatus().equals(OrderStatus.NEW)) {
                model.addAttribute("noneditable", true);
                return "redirect:orders";
            }
        }
        if (!orderDTO.getPositions().isEmpty()) {
            for (PositionDTO position : orderDTO.getPositions().keySet()) {
                orderDTO.getPositionsNamesList().add(position.getArticul());
            }
        }
        model.addAttribute("ordercost", orderDTO.getCost());
        model.addAttribute("positions", orderDTO.getPositions());
        model.addAttribute("orderId", orderDTO.getOrderId());
        return "user/ordersedit";
    }

    @RequestMapping(value = "/orderaction", method = RequestMethod.POST)
    public String basketaction(Model model, OrderDTO order, String action, HttpServletRequest request) {
        OrderDTO orderDTO = orderService.getOrderById(order.getOrderId());
        if (!order.getPositionsNamesList().isEmpty()) {
            if (action.equals("Delete positions")) {
                for (Integer articul : order.getPositionsNamesList()) {
                    orderService.deleteFromOrderById(order.getOrderId(), articul);
                    orderDTO.setCost(orderService.getOrderCostByOrderId(order.getOrderId()));
                }
                if (orderService.getOrderCostByOrderId(order.getOrderId()) == 0) {
                    orderService.deleteOrderById(order.getOrderId());
                }
            }
        }
        if (action.equals("Save Order")) {
            for (PositionDTO position : orderDTO.getPositions().keySet()) {
                String positionNumber = request.getParameter(position.getArticul().toString());
                Integer number = Integer.parseInt(positionNumber);
                if (number > 0) {
                    if (!Objects.equals(orderDTO.getPositions().get(position), number)) {
                        orderService.changePositionNumberByArticul(position.getArticul(), order.getOrderId(), number);
                    }
                }
            }
            orderDTO.setCost(orderService.getOrderCostByOrderId(order.getOrderId()));
        }
        model.addAttribute("order", order);
        model.addAttribute("positionsArticul", orderDTO.getPositionsNamesList());
        return "redirect: orders";
    }
}
