package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.cops2002.service.UserService;
import ru.mail.cops2002.service.models.UserDTO;
import ru.mail.cops2002.web.validator.UserValidator;

@Controller
public class RegisterController {

    private final UserService userService;
    private final UserValidator userValidator;

    @Autowired
    public RegisterController(UserService userService, UserValidator userValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute("user") UserDTO user, BindingResult result, Model model, String passwordrepeat) {
        if (passwordrepeat.equals(user.getPassword())) {
            userValidator.validate(user, result);
            if (!result.hasErrors()) {
                userService.saveUser(user);
                return "redirect:/login";
            } else {
                return "registration";
            }
        }
        model.addAttribute("passworderror", true);
        return "registration";
    }

    @RequestMapping(value = "/back", method = RequestMethod.POST)
    public String showWelcomePage() {
        return "login";
    }
}
