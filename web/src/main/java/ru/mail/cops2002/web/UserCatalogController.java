package ru.mail.cops2002.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mail.cops2002.repository.models.User;
import ru.mail.cops2002.service.BasketService;
import ru.mail.cops2002.service.models.AppUserPrincipal;
import ru.mail.cops2002.service.models.BasketDTO;
import ru.mail.cops2002.service.models.PositionDTO;
import ru.mail.cops2002.service.util.UserConverter;

@Controller
@RequestMapping(value = "/user")
public class UserCatalogController {

    private final BasketService basketService;


    @Autowired
    public UserCatalogController(BasketService basketService) {
        this.basketService = basketService;
    }


    @RequestMapping(value = "/basketadd", method = RequestMethod.POST)
    public String addPositions(@ModelAttribute PositionDTO position, @RequestParam String submit, Integer number, Integer basketcost) {
        {
            AppUserPrincipal userPrincipal = (AppUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = userPrincipal.getUser();
            BasketDTO basketDTO = new BasketDTO();
            if (submit.equals("add to basket")) {
                if (number > 0) {
                    basketDTO.getPositions().put(position, number);
                    basketcost = basketcost + (position.getCost() * number);
                } else {
                    basketDTO.getPositions().put(position, 1);
                    basketcost = basketcost + position.getCost();
                }
            }
            if (submit.equals("delete from basket")) {
                basketService.deleteFromBasketByArticul(position.getArticul(), basketService.getBasketByUserId(user.getId()).getBasketId());
                basketcost = basketcost - (position.getCost() * number);
            }
            basketDTO.setBasketCost(basketcost);
            basketDTO.setUserDTO(UserConverter.convert(user));
            basketDTO.setUserId(user.getId());
            basketDTO.setBasketId(basketService.getBasketByUserId(user.getId()).getBasketId());
            basketService.saveBasket(basketDTO);
            return "redirect: catalog";
        }
    }
}
